#ifndef PROGRAMMING_CPP_2K_SHAPE2D_H
#define PROGRAMMING_CPP_2K_SHAPE2D_H

#include <iostream>
#include <string>

class Shape2D {
public:
    virtual ~Shape2D();

    virtual double area() const = 0;
    virtual double perimeter() const = 0;
    virtual std::string to_string() const;
    friend std::ostream& operator << (std::ostream &, const Shape2D&);
};


#endif //PROGRAMMING_CPP_2K_SHAPE2D_H
