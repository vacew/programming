#ifndef PROGRAMMING_CPP_2K_ROMB_H
#define PROGRAMMING_CPP_2K_ROMB_H

#include "shape2d.h"

class Romb: public Shape2D {
private:
    double a;
    double angle;
public:
    Romb(double a, double angle);

    virtual ~Romb();

    double area() const override;

    double perimeter() const override;

    std::string to_string() const override;
};


#endif //PROGRAMMING_CPP_2K_ROMB_H
