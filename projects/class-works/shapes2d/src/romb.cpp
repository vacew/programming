#include "romb.h"
#include <cmath>
#include <sstream>

Romb::Romb(double a, double angle)
        : a(a), angle(angle)
{}

Romb::~Romb() {

}

double Romb::area() const {
    return a*a*std::sin(angle);
}

double Romb::perimeter() const {
    return 4*a;
}

std::string Romb::to_string() const {
    std::stringstream sin;

    sin << "Romb{";
    sin << "a: " << this->a << "; angle: " << angle;
    sin << "; perimeter: " << this->perimeter();
    sin << "; area: " << this->area();
    sin << "}";

    return sin.str();
}