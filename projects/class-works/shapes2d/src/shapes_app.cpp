#include <iostream>
#include <cmath>
#include <memory>

#include "shape2d.h"
#include "romb.h"

#define _USE_MATH_DEFINITIONS

int main()
{
    Romb r1(11, M_PI / 6);

    std::cout << r1 << std::endl;
    std::shared_ptr<Shape2D> pshape2(new Romb(10, M_PI/4));

    std::cout << *pshape2 << std::endl;

    const Shape2D& rshape = Romb(5.5, M_PI/8);

    std::cout << rshape << std::endl;
    return 0;
}
