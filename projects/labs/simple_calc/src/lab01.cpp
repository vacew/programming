
#include <iostream>

#define _USE_MATH_HELPERS_

#include <cmath>
#include "common_math.h"

using namespace std;

int main(int argc, char** argv)
{
    double x = 2, a = -1.3, b = 21.05;
    double y = std::sqrt(x*a-b)/SQR(b*x-2);
    
    cout << "Result: " << y << endl;
    return 0;
}
