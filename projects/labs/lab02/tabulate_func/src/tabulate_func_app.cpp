
#include "tabulate_func.h"

using namespace std;

/*
 * Lab 02 2
 * Variant 34
 */

int main(int argc, char** argv)
{
    double a = common::math::generate_random(-10.0, 10.0);
    double b = common::math::generate_random(a + 0.1, a + 10.0);
    
    double c = common::math::generate_random(-10.0, 10.0);
    double d = common::math::generate_random(c + 0.1, c + 10.0);

    int nx = common::math::generate_random(10, 20);
    int ny = common::math::generate_random(10, 20);
    
    tabulate_func(func, a, b, c, d, nx, ny);

    return 0;
}


