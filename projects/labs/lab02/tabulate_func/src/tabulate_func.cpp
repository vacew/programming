#include "tabulate_func.h"

double func(double x, double y)
{
	return x + y;
}

void tabulate_func(common::math::func2_t f, double a, double b, double c, double d, int nx, int ny)
{
	double hx = (b - a) / nx;
	double hy = (d - c) / ny;
	
	std::cout << "y/x";

	for (double x = a; x <= b; x += hx)
	{
		std::cout << x << (x <= b - hx) ? "\t" : "\n";
	}

	for (double y = c; y <= d; y += hy)
	{
		std::cout << y << "\t";
		for (double x = a; x <= b; x += hx)
		{
			double z = f(x, y);
			std::cout << z << (x <= b - hx) ? "\t" : "\n";
		}
	}
}