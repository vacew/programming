#ifndef PROGRAMMING_CPP_2K_COMPUTE_SEQUENCE_H
#define PROGRAMMING_CPP_2K_COMPUTE_SEQUENCE_H

#include <iostream>

#define _USE_MATH_HELPERS_

#include "common_math.h"

double func(double, double);
void tabulate_func(common::math::func2_t, double, double, double, double, int nx = 10, int ny = 10);
#endif //PROGRAMMING_CPP_2K_COMPUTE_SEQUENCE_H
