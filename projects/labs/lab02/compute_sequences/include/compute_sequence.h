#ifndef PROGRAMMING_CPP_2K_COMPUTE_SEQUENCE_H
#define PROGRAMMING_CPP_2K_COMPUTE_SEQUENCE_H

#include <iostream>

#define _USE_MATH_HELPERS_

#include "common_math.h"

double sequence_multiply(double, int);
double etalon_func(double);
#endif //PROGRAMMING_CPP_2K_COMPUTE_SEQUENCE_H
