
#include "compute_sequence.h"

using namespace std;

/*
 * Lab 02
 * Variant 34
 */

int main(int argc, char** argv)
{
    int N;
    if (argc > 1)
    {
        N = atoi(argv[1]);
    } else {
        N = common::math::generate_random(100, 1001);
    }
    
    double x = common::math::generate_random(-1.0, 1.0);
    cout << "x=" << x << endl;
    double a = -x/3;
    double S = common::math::compute_sequence_value(x, a, sequence_multiply, 1, N);
    double y = etalon_func(x), eps = DEVIATE_EPS(y, S);
    cout << "y=" << y << "; S=" << S << "; eps=" << eps << endl;
    return 0;
}


