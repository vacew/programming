#include <iostream>
#include <memory>
#include "shapes.h"
#include "rectangles.h"
#include "ellipse.h"

using namespace std;
using namespace com::graph::lib;

int main() {
    shared_ptr<GraphContext> context(new ConsoleGraphContext());

    shared_ptr<Shape2D> shape1(new Rectangle2D(context.get(), 100, 200, 120, 80));
    shape1->draw();

    shared_ptr<Shape2D> shape2(new Ellipse2D(context.get(), 300, 250, 100, 200));
    shape2->draw();

    return 0;
}

