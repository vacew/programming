#include "rectangles.h"

namespace com {
    namespace graph {
        namespace lib {
            Rectangle2D::~Rectangle2D() {

            }

            Rectangle2D::Rectangle2D(GraphContext *ctx) : Shape2D(ctx) {
            }

            Rectangle2D::Rectangle2D(GraphContext *ctx, int _x, int _y, int _w, int _h)
                    : Shape2D(ctx), x(_x), y(_y), width(_w), height(_h) {
            }

            void Rectangle2D::draw() {
                int xR = this->x + this->width;
                int yR = this->y + this->height;
                ctx->rect(this->x, this->y, xR, yR);
            }

            int Rectangle2D::getX() const {
                return x;
            }

            int Rectangle2D::getY() const {
                return y;
            }

            int Rectangle2D::getWidth() const {
                return width;
            }

            int Rectangle2D::getHeight() const {
                return height;
            }

            void Rectangle2D::move(int dx, int dy) {
                this->x += dx;
                this->y += dy;
                this->draw();
            }

            void Rectangle2D::scale(int factor) {
                this->width = this->height *= factor;
                this->draw();
            }

            double Rectangle2D::area() const {
                return this->width * this->height;
            }

            double Rectangle2D::perimeter() const {
                return 2 * (this->width + this->height);
            }
        }
    }
}