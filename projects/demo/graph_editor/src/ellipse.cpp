#include "ellipse.h"
#include <cmath>
namespace com {
    namespace graph {
        namespace lib {
            Ellipse2D::Ellipse2D(GraphContext *ctx) : Shape2D(ctx) {}

            Ellipse2D::~Ellipse2D() {

            }

            Ellipse2D::Ellipse2D(GraphContext *ctx, int x, int y, int a, int b)
                    : Shape2D(ctx), x(x), y(y), a(a), b(b)
            {}

            int Ellipse2D::getX() const {
                return x;
            }

            int Ellipse2D::getY() const {
                return y;
            }

            int Ellipse2D::getA() const {
                return a;
            }

            int Ellipse2D::getB() const {
                return b;
            }

            void Ellipse2D::draw() {
                int xL = this->x - this->a;
                int yL = this->y - this->b;

                int xR = this->x + this->a;
                int yR = this->y + this->b;
                this->ctx->ellipse(xL, yL, xR, yR);
            }

            void Ellipse2D::move(int dx, int dy) {
                this->x += dx;
                this->y += dy;
                this->draw();
            }

            void Ellipse2D::scale(int factor) {
                this->a = this->b *= factor;
                this->draw();
            }

            double Ellipse2D::area() const {
                return M_PI * this->a *this->b;
            }

            double Ellipse2D::perimeter() const {
                return M_PI_2*(a + b);//TODO more precision
            }
        }
    }
}