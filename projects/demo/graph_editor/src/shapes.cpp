#include "shapes.h"
#include <cmath>
#include <iostream>

namespace com {
    namespace graph {
        namespace lib {

            void ConsoleGraphContext::rect(int x1, int y1, int x2, int y2, int color, int bgColor) {
                std::cout << "Rect {" << x1 << ", " << y1 << ", " << x2 << ", " << y2 << "}" << std::endl;
            }

            void ConsoleGraphContext::ellipse(int x1, int y1, int x2, int y2, int color, int bgColor) {
                std::cout << "Ellipse {" << x1 << ", " << y1 << ", " << x2 << ", " << y2 << "}" << std::endl;
            }

            void ConsoleGraphContext::fill(int x, int y, int color) {
                std::cout << "Fill shape {" << x << ", " << y << ", color: " << color  << "}" << std::endl;
            }

            int ConsoleGraphContext::clientWidth() const {
                return 80;
            }

            int ConsoleGraphContext::clientHeight() const {
                return 25;
            }

            Shape2D::~Shape2D() {

            }

            Shape2D::Shape2D(GraphContext *ctx) : ctx(ctx) {}

            int Shape2D::getColor() const {
                return color;
            }

            void Shape2D::setColor(int color) {
                Shape2D::color = color;
            }

            int Shape2D::getBgColor() const {
                return bgColor;
            }

            void Shape2D::setBgColor(int bgColor) {
                Shape2D::bgColor = bgColor;
            }
        }
    }
}
