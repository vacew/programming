#ifndef PROGRAMMING_CPP_2K_ELLIPSE_H
#define PROGRAMMING_CPP_2K_ELLIPSE_H

#include "shapes.h"

namespace com {
    namespace graph {
        namespace lib {
            class Ellipse2D : public Shape2D {
            public:
                Ellipse2D(GraphContext *ctx);

                Ellipse2D(GraphContext *ctx, int x, int y, int a, int b);

                virtual ~Ellipse2D();

                void draw() override;

                void move(int, int) override;

                void scale(int) override;

                double area() const override;

                double perimeter() const override;

            public:
                int getX() const;

                int getY() const;

                int getA() const;

                int getB() const;

            private:
                int x;
                int y;
                int a;
                int b;
            };
        }
    }
}

#endif //PROGRAMMING_CPP_2K_ELLIPSE_H
