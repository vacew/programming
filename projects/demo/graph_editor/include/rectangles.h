#ifndef PROGRAMMING_CPP_2K_RECTANGLES_H
#define PROGRAMMING_CPP_2K_RECTANGLES_H

#include "shapes.h"

namespace com {
    namespace graph {
        namespace lib {
            class Rectangle2D : public Shape2D {
            public:
                Rectangle2D(GraphContext *);

                Rectangle2D(GraphContext *, int, int, int, int);

                virtual ~Rectangle2D();

                virtual void draw();

                virtual void move(int, int);

                virtual void scale(int);

                double area() const override;

                double perimeter() const override;

            public:
                int getX() const;

                int getY() const;

                int getWidth() const;

                int getHeight() const;

            private:
                int x;
                int y;
                int width;
                int height;
            };
        }
    }
}
#endif //PROGRAMMING_CPP_2K_RECTANGLES_H
