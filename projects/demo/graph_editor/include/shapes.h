#ifndef PROGRAMMING_CPP_2K_SHAPES_H
#define PROGRAMMING_CPP_2K_SHAPES_H

namespace com {
    namespace graph {
        namespace lib {
            class GraphContext {
            public:
                virtual void rect(int x1, int y1,
                                  int x2, int y2,
                                  int color = -1, int bgColor = -1) = 0;

                virtual void ellipse(int x, int y, int x2, int y2,
                                     int color = -1, int bgColor = -1) = 0;

                virtual void fill(int x, int y, int color) = 0;

                virtual int clientWidth() const = 0;

                virtual int clientHeight() const = 0;
            };

            class ConsoleGraphContext : public GraphContext {
            public:
                void rect(int x1, int y1, int x2, int y2, int color, int bgColor) override;

                void ellipse(int x, int y, int x2, int y2, int color, int bgColor) override;

                void fill(int x, int y, int color) override;

                int clientWidth() const override;

                int clientHeight() const override;
            };

            class Shape2D {
            public:
                Shape2D(GraphContext *ctx);

                virtual ~Shape2D();

                int getColor() const;

                void setColor(int color);

                int getBgColor() const;

                void setBgColor(int bgColor);

                virtual void draw() = 0;

                virtual void move(int, int) = 0;

                virtual void scale(int) = 0;

                virtual double area() const = 0;

                virtual double perimeter() const = 0;

            protected:
                GraphContext *ctx;
            private:
                int color;
                int bgColor;
            };
        }
    }
}

#endif //PROGRAMMING_CPP_2K_SHAPES_H
