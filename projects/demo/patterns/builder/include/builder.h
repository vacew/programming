#ifndef BUILDER_H
#define BUILDER_H

#include <string>

namespace demo {
    namespace patterns {
        namespace builder {
            class JobDetail {
            public:
                JobDetail(const std::string &name, const std::string &address);

                virtual ~JobDetail();

                std::string to_string() const;
            private:
                std::string name;
                std::string address;
            };

            class PersonBuilder;

            class Person
            {
            public:

                virtual ~Person();

                std::string get_name() const;
                std::string get_address() const;
                std::string get_birth_date() const;

                const JobDetail* get_job_detail() const;

                int get_age() const;
                std::string to_string() const;
            protected:
                void set_name(const std::string &name);

                void set_address(const std::string &address);

                void set_birth(const std::string &birthDate);

                void set_job_detail(JobDetail* jobDetail);
            private:
                std::string name;
                std::string address;
                std::string birth_date;
                JobDetail *jobDetail;
                Person() {}
            public:
                friend std::ostream& operator<<(std::ostream& os, const Person& p);
                friend class PersonBuilder;
            };

            class PersonBuilder {
            public:
                PersonBuilder();
                Person build();

                PersonBuilder& name(const std::string& name);
                PersonBuilder& address(const std::string& address);
                PersonBuilder& birth(const std::string& birth);
                PersonBuilder& job_details(JobDetail* jobDetail);

            private:
                Person person;
            public:
                friend class Person;
            };
        }
    }
}

#endif // BUILDER_H
