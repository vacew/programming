#include "builder.h"
#include <sstream>
#include <iostream>

namespace demo {
    namespace patterns {
        namespace builder {

            Person::~Person() {
                std::cout << "Destruct person" << std::endl;
            }

            int Person::get_age() const {
                return 0;
            }

            std::string Person::get_name() const {
                return this->name;
            }

            std::string Person::get_address() const {
                return this->address;
            }

            std::string Person::get_birth_date() const {
                return this->birth_date;
            }

            std::ostream &operator<<(std::ostream &os, const Person &p) {
                os << p.to_string();
                return os;
            }

            std::string Person::to_string() const {
                std::stringstream sin;
                sin << "Person{name: " << name << "; address: " << address << "; birth: ";
                sin << birth_date << "; jobDetails: ";
                sin << (jobDetail ? jobDetail->to_string() : "") << "}";
                return sin.str();
            }

            const JobDetail* Person::get_job_detail() const {
                return jobDetail;
            }

            void Person::set_name(const std::string &name) {
                Person::name = name;
            }

            void Person::set_address(const std::string &address) {
                Person::address = address;
            }

            void Person::set_birth(const std::string &birthDate) {
                birth_date = birthDate;
            }

            void Person::set_job_detail(JobDetail* jobDetail) {
                this->jobDetail = jobDetail;
            }

            JobDetail::JobDetail(const std::string &name,
                                 const std::string &address)
                                 : name(name), address(address) {}

            std::string JobDetail::to_string() const {
                std::stringstream sin;
                sin << "JobDetails: {name: " << name << "; address: " << address << "}";
                return sin.str();
            }

            JobDetail::~JobDetail() {
                std::cout << "Destruct job detail" << std::endl;
            }


            PersonBuilder::PersonBuilder() {
            }

            Person PersonBuilder::build() {
                return person;
            }

            PersonBuilder &PersonBuilder::name(const std::string &name) {
                person.set_name(name);
                return *this;
            }

            PersonBuilder &PersonBuilder::address(const std::string &address) {
                person.set_address(address);
                return *this;
            }

            PersonBuilder &PersonBuilder::birth(const std::string &birth) {
                person.set_birth(birth);
                return *this;
            }

            PersonBuilder &PersonBuilder::job_details(JobDetail* jobDetail) {
                person.set_job_detail(jobDetail);
                return *this;
            }
        }
    }
}
