#include <iostream>
#include <memory>

#include "builder.h"

using namespace std;
using namespace demo::patterns;

int main()
{
    builder::JobDetail jobDetail("Microsoft", "Sietl");
    builder::Person p1 = builder::PersonBuilder()
            .name("Fill Jons")
            .address("14 Central avenu, New Yourk, USA")
            .birth("22/04/1991")
            .job_details(&jobDetail)
            .build();

    cout << p1 << endl;
    {
        builder::Person p2 = builder::PersonBuilder()
                .name("Mikle Collins")
                .address("12 Central park, New Jersey, USA")
                .build();

        cout << p2 << endl;
    }

    return 0;
}
