#ifndef SIGNLETON_H
#define SIGNLETON_H

#include <string>
#include <memory>

namespace demo {
    namespace patterns {
        namespace singleton {

            class Logger {
            public:
                std::ostream& print(const std::string&);
            public:
                static std::shared_ptr<Logger> get_instance(std::ostream&);
            private:
                static std::shared_ptr<Logger> logger;

                std::ostream& os;
                Logger(std::ostream& os): os(os) {};
            };
        }
    }
}

#endif // SIGNLETON_H
