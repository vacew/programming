#include <iostream>
#include <memory>

#include "singleton.h"

using namespace std;
using namespace demo::patterns;

int main()
{
    cout << "===== Singleton =====" << endl;

    {
        std::shared_ptr<singleton::Logger> logger = singleton::Logger::get_instance(cout);
        logger->print("Message 134253") << "\n";
    }

    {
        std::shared_ptr<singleton::Logger> logger = singleton::Logger::get_instance(cout);
        logger->print("Message ###########") << "\n";
    }

    return 0;
}
