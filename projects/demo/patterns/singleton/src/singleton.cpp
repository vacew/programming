#include "singleton.h"
namespace demo {
    namespace patterns {
        namespace singleton {
            std::shared_ptr<Logger> Logger::logger;
            
            std::shared_ptr<Logger> Logger::get_instance(std::ostream& os) {
                if (!logger) {
                    logger = std::shared_ptr<Logger>(new Logger(os));
                }
                return logger;
            }

            std::ostream& Logger::print(const std::string &data) {
                os << data;
                return os;
            }
        }
    }
}