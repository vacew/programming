#ifndef INHERITS_H
#define INHERITS_H

#include <string>
#include <iostream>

using namespace std;

namespace demo {
    namespace inherits {

        class A {
        public:
            A(int m) : _m(m) {
                cout << "Ctr A" << endl;
            }

            virtual ~A() {
                cout << "Dtr A" << endl;
            }

            int scale(const int& s) const {
                return s * this->_m;
            }

            int getM() const {
                return _m;
            }

        protected:
        private:
            int _m;
        };

        class B : public A {
        public:
            B(int m, int n) : A(m), _n(n) {
                cout << "Ctr B" << endl;
            }

            virtual ~B() {
                cout << "Dtr B" << endl;
            }

            int inverse(const int& s) const {
                return getM() / s;
            }

            int multInvers(const int& s) const {
                return getM() / s * this->_n / s;
            }

            int getN() const {
                return _n;
            }

        private:
            int _n;
        };

        class C : public B {};
        class D : public B {};

        class F : public C {};
        class G : public F, public D {
        };

    }
}

#endif // INHERITS_H
