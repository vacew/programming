#include <iostream>
#include "inherits.h"

using namespace std;
using namespace demo::inherits;

int main()
{
    A *a = new B(2, 5);
    delete a;

    B b(2, 4);

    A &ra = b;
    b.getN();
    return 0;
}
