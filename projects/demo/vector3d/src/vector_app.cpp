#include <iostream>

#include "vector3d.h"

using namespace std;
using namespace demo::math;

int main()
{
    double data1[] = {-2, 2, -4};
    double data2[] = {2, -2, 5};

    vector3d::Vector3D v1 = data1;

    vector3d::Vector3D v2 = data2;

    cout << v1.to_string() << endl;
    cout << v2.to_string() << endl;
    cout << "=================" << endl;

    cout << v1.sum(v2).to_string() << endl;
    cout << v2.subs(v1).to_string() << endl;
    cout << "=================" << endl;

    cout << v1.mult(-2).to_string() << endl;
    cout << v2.mult(2).to_string() << endl;
    cout << "=================" << endl;

    cout << v1.scalarMult(v2) << endl;
    cout << v2.vectorMult(v1).to_string() << endl;
    cout << "=================" << endl;

    cout << v2.angle(v1) << endl;
    cout << v1.angle(v2) << endl;
    cout << "=================" << endl;

    return 0;
}
