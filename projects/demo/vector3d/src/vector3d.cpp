#include "vector3d.h"
#include <cmath>
#include <sstream>

namespace demo {
    namespace math {
        namespace vector3d {
            Vector3D::Vector3D(const double data[3])
            {
                this->data[0] = data[0];
                this->data[1] = data[1];
                this->data[2] = data[2];
            }

            Vector3D::~Vector3D()
            {
            }

            double Vector3D::length() const
            {
                return std::sqrt(data[0]*data[0] + data[1]*data[1] + data[2]*data[2]);
            }

            double Vector3D::angleX() const
            {
                return std::acos(data[0]/length());
            }

            double Vector3D::angleY() const
            {
                return std::acos(data[1]/length());
            }

            double Vector3D::angleZ() const
            {
                return std::acos(data[2]/length());
            }

            double Vector3D::scalarMult(const Vector3D& v) const
            {
                return data[0]*v.data[0] + data[1]*v.data[1] + data[2]*v.data[2];
            }

            Vector3D Vector3D::vectorMult(const Vector3D& v) const
            {
                double result[3] = {
                        data[1]*v.data[2] - data[2]*v.data[1],
                        data[2]*v.data[0] - data[0]*v.data[2],
                        data[0]*v.data[1] - data[1]*v.data[0]
                };

                return Vector3D { result };
            }

            Vector3D Vector3D::sum(const Vector3D& v) const
            {
                double result[3] =
                        {
                                data[0] + v.data[0],
                                data[1] + v.data[1],
                                data[2] + v.data[2]
                        };

                return Vector3D {result};
            }

            Vector3D Vector3D::subs(const Vector3D& v) const
            {
                double result[3] =
                        {
                                data[0] - v.data[0],
                                data[1] - v.data[1],
                                data[2] - v.data[2]
                        };

                return Vector3D {result};
            }

            Vector3D Vector3D::mult(const double& a) const
            {
                double result[3] =
                        {
                                a*data[0],
                                a*data[1],
                                a*data[2]
                        };

                return Vector3D {result};
            }

            double Vector3D::angle(const Vector3D& v) const
            {
                return std::acos(scalarMult(v)/length()/v.length());
            }

            std::string Vector3D::to_string() const
            {
                std::stringstream sin;

                sin << "Vector3D{a=" << data[0] << "; b=" << data[1] << "; c=" << data[2] << "; L=" << length() << "}";

                return sin.str();
            }

        }
    }
}
