#include <iostream>
#include "heap.h"
using namespace std;

void massive(int* arr, int& s)
{
    cout << "1)" << endl;
    cout << "Your array is : " << endl;
    for (int i = 0; i < s; i++)
    {
        arr[i] = rand() % 3000 - 1000;
        cout << arr[i] << " ";
    }
    cout << endl;
    cout << endl;
}
void negative(int* arr, int& s, int& n)
{
    cout << "2)" << endl;
    int number = 0;
    cout << "Enter your negative value : ";
    cin >> n;
    for (int i = 0; i < s; i++)
    {
        if (arr[i] > n&& arr[i] < 0)
            number++;
    }
    cout << "The number of negative numbers that bigger than " << n << ": " << number << endl;
    cout << endl;
}
void l(int* arr, int& s)
{
    cout << "3,4)" << endl;
    double l;
    for (int i = 0; i < s; i++)
    {
        l = pow(arr[i], 2);
    }
    cout << "Your l : " << l << endl;
    cout << endl;
}
void minandmax(int* arr, int& s, int& minim, int& maximum)
{
    cout << "5)" << endl;

    for (int i = 0; i < s; i++)
    {
        if (minim > arr[i])
            minim = arr[i];
        if (maximum < arr[i])
            maximum = arr[i];
    }
    cout << "Your min number : " << minim << endl;
    cout << "Your max number : " << maximum << endl;
}
void findelement(int* arr, int& s, int& n)
{
    cout << "6)" << endl;
    cout << "Enter your number : ";
    cin >> n;
    for (int i = 0; i < s; i++)
    {
        if (n == arr[i])
        {
            cout << "Your number element : " << i << endl;
            break;
        }
    }
    cout << endl;
    cout << endl;
}
void bubble(int* arr, int& s, int& n)
{
    cout << "7)" << endl;
    n = 0;
    for (int i = 0; i < s - 1; i++)
    {
        for (int j = 0; j < s - i - 1; j++)
        {
            if (arr[j] > arr[j + 1])
            {
                n = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = n;
            }
        }
    }
    for (int i = 0; i < s; i++)
    {
        cout << arr[i] << " ";
    }
    cout << endl;
    cout << endl;
}
void binfindelem(int* arr, int& s, int& n)
{
    cout << "8)" << endl;
    cout << "Enter your value : ";
    cin >> n;
    int L = 0;
    int r = s - 1;
    bool flag = false;
    int mid;
    while (L <= r && flag != true)
    {
        mid = (L + r) / 2;
        if (arr[mid] == n) flag = true;
        if (arr[mid] > n) r = mid - 1;
        else L = mid + 1;
    }
    cout << "Your number element : " << mid;
    cout << endl;
    cout << endl;
}
void matrix(int& minim, int& maximum)
{
    int M;
    int N;
    cout << "Enter the number of the columns and the rows in your matrix : ";
    cin >> M;
    cin >> N;
    int f[M][N];

    cout << "9)" << endl;
    minim = f[0][0];
    maximum = f[0][0];
    for (int i = 0; i < M; i++)
    {
        for (int j = 0; j < N; j++)
        {
            f[i][j] = rand() % 200 - 100;
            cout << f[i][j] << " ";
            if (minim > f[i][j])
                minim = f[i][j];
            if (maximum < f[i][j])
                maximum = f[i][j];
        }
        cout << endl;
    }
    cout << "Minimum of your matrix : " << minim << endl;
    cout << "Maximum of your matrix : " << maximum << endl;
}
