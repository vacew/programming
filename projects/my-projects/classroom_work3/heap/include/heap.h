#ifndef HEAP_H_INCLUDED
#define HEAP_H_INCLUDED

#include <ctime>
#include <stdlib.h>
#include <stdio.h>
#include <cmath>

void massive(int*, int&);
void negative(int*, int&, int&);
void l(int*, int&);
void minandmax(int*, int&, int&, int&);
void findelement(int*, int&, int&);
void bubble(int*, int&, int&);
void binfindelem(int*, int&, int&);
void matrix(int&, int&);


#endif // HEAP_H_INCLUDED
