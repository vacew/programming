#include "4.h"

using namespace std;

/*
 * classroom work 4
 * 
 */

int main()
{
    int n;
    cout << "Enter your number : ";
    cin >> n;
    for (int i = 2; i < n; i++)
    {
        if (n % i == 0)
        {
            cout << "This is not a prime number.";
            return 0;
        }
    }
    cout << "It's ok. Your number is prime.";
    return 0;
}
