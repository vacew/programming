#include "6.h"

using namespace std;

/*
 * classroom work 6
 * 
 */

int main()
{
    int a, b, c;
    cout << "Enter your 3 sides : ";
    cin >> a >> b >> c;
    if (a + b > c&& a + c > b&& b + c > a)
    {
        if ((a * a == b * b + c * c) || (b * b == a * a + c * c) || (c * c == b * b + a * a))
            cout << "It's right triangle.";
        if (a == b && a == c && b == c)
        {
            cout << "It's equilateral triangle.";
        }
        else if (a == b || a == c || b == c)
            cout << "It's isosceles triangle.";
    }
    else
    {
        cout << "It doesn't exist.";
    }
    return 0;
}
