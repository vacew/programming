#include "9.h"

using namespace std;

/*
 * classroom work 9
 * 
 */

int main()
{
    setlocale(LC_ALL, "Russian");
    help();
    for (;;)
    {
        if (_kbhit)
        {
            switch (_getch())
            {
            case '1': cout << row(); break;
            case '2': cout << func(); break;
            case '3': eps(); break;
            case '4': t_func1(); break;
            case '5': t_func2(); break;
            case 'q': return 0;
            case 'h': help(); break;
            }
        }
    }
    return 0;
}
