#include "9.h"
#include <iostream>

using namespace std;

double row()
{
    system("cls");
    double x = 0.1;
    double s = 0;
    int N = 10;
    double a = x * cos(M_PI / 3);
    for (int n = 1; n <= N; n++)
    {
        s += a;
        double q = (n * x * cos((n + 1) * M_PI / 3)) / (cos(n * M_PI / 3) * (n + 1));
        a *= q;
    }
    return s;
}

double func()
{
    system("cls");
    double x = 0.1;
    double result;
    double v;
    v = log(1 - 2 * x * cos(M_PI / 3) + pow(x, 2));
    result = -0.5 * v;
    return result;
}

void eps()
{
    system("cls");
    cout << "����������� � ����� ���� ���������.";
}

void t_func1()
{
    system("cls");
    double a = -M_PI * 0.5, b = M_PI * 0.5;
    double y = 0;
    double minimum = 0;
    double maximum = 0;
    cout << "X" << "\t\t" << "func" << endl;
    for (double x = a; x <= b; x += 0.3)
    {
        cout << x << "  ";
        cout << "\t";
        if (x <= -M_PI * 0.25)
        {
            y = sin(pow(x, 2) - 1);
            cout << y << endl;
        }
        if (x <= 0 && x >= -M_PI * 0.25)
        {
            y = asin(x * sin(2 * x));
            cout << y << endl;
        }
        if (x >= 0 && x <= b)
        {
            y = sin(x - 2);
            cout << y << endl;
        }
        if (minimum > y)minimum = y;
        if (maximum < y)maximum = y;
    }
    cout << "Min : " << minimum << " Max : " << maximum << endl;
}

void t_func2()
{
    system("cls");
    double a = 0;
    double b = M_PI;
    double c = -M_PI;
    double d = 0;
    double func = 0;
    cout.width(15);
    for (double x = a; x <= b; x += 0.2)
    {
        cout << x;
        cout.width(10);
    }
    cout << endl;
    cout << "\n";
    for (double y = c; y <= d; y += 0.1)
    {
        cout << y;
        for (double x = a; x <= b; x += 0.2)
        {
            double u = pow(abs(x + y), 1.0 / 2);
            if (x + y < 0) u *= -1;
            func = (cosh(u) / cos(2 * x - y));
            cout.width(10);
            cout << func;
        }
        cout << endl;
    }

}

void help()
{
    system("cls");
    cout << "������������ ������: 2" << endl;
    cout << "�������: 12" << endl;
    cout << "������� ������� ������ �������������" << endl;
    cout << "[1] \t ��������� ����� ����. " << endl;
    cout << "[2] \t ��������� ������ �������� �������. " << endl;
    cout << "[3] \t ��������� ����������� � �����. " << endl;
    cout << "[4] \t ��������������� ������� � ����� ����������. " << endl;
    cout << "[5] \t ��������������� ������� � ����� �����������. " << endl;
    cout << "[q] \t ����� � ���������. " << endl;
    cout << "[h] \t ������. " << endl;
}