#ifndef PROG_H_INCLUDED
#define PROG_H_INCLUDED

# define M_PI 3.14159265358979323846

#include <cmath>
#include <stdio.h>
#include <conio.h>
#include <windows.h>

double row();
double func();
void eps();
void t_func1();
void t_func2();
void help();

#endif // PROG_H_INCLUDED
