#include "10.h"

using namespace std;

/*
 * classroom work
 *
 */

int main()
{
    int n = 0;
    int result = 1;
    cout << "Write your number : ";
    cin >> n;
    for (int i = 1; i <= n; i++)
    {
        result *= i;
    }
    cout << "Your factorial : " << result << endl;
    return 0;
}
