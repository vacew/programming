#include "7.h"

int f(int xa, int ya, int xb, int yb, int xc, int yc, int xd, int yd)
{
    return (xd - xa) * (yb - ya) - (yd - ya) * (xb - xa);
}