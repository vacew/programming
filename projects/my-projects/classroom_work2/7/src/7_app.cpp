#include "7.h"

using namespace std;

/*
 * classroom work 7
 * 
 */

int main()
{
    int xa, ya, xb, yb, xc, yc, xd, yd;
    cout << "Enter your coordinates  : " << endl;
    cout << "A = "; cin >> xa >> ya;
    cout << "B = "; cin >> xb >> yb;
    cout << "C = "; cin >> xc >> yc;
    cout << "D = "; cin >> xd >> yd;
    if (f(xa, ya, xb, yb, xc, yc, xd, yd) && f(xb, yb, xc, yc, xa, ya, xd, yd) &&
        f(xc, yc, xa, ya, xb, yb, xd, yd))
    {
        cout << "Your point belongs to the triangle.";
    }
    else
    {
        cout << "Your point doesn't belong to the triangle.";
    }
    return 0;
}
