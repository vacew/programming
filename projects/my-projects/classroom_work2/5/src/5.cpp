#include "5.h"

using namespace std;

/*
 * classroom work 5
 * 
 */

int main()
{
    int a, b, c, d;
    cout << "Enter your first dice : ";
    cin >> a >> b;
    cout << "Enter your second dice : ";
    cin >> c >> d;
    if (a == c || a == d || b == c || b == d)
    {
        cout << "You can connect it.";
    }
    else
    {
        cout << "You can't connect it.";
    }
    return 0;
}
