#include "1.h"

using namespace std;

/*
 * classroom work 1
 * 
 */

int main()
{
    int R = 1;
    int X0 = 0, Y0 = 0, X1 = -1, Y1 = 0;
    int x, y;
    cout << "Enter your x : ";
    cin >> x;
    cout << "Enter your y : ";
    cin >> y;
    if (x < (X0 + R) && x < (X1 + R))
    {
        if (y < (Y0 + R) && y < (Y1 + R))
        {
            cout << "Your point M belongs to the circle intersection region";
        }
        else
        {
            cout << "Your point M doesn't belong to the circle intersection region";
        }
    }
    else
    {
        cout << "Your point M doesn't belong to the circle intersection region";
    }
    return 0;
}
