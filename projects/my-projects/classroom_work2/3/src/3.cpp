#include "3.h"

using namespace std;

/*
 * classroom work 3
 * 
 */

int main()
{
    int a, b;
    int nsk;
    cout << "Enter first number : ";
    cin >> a;
    cout << " Enter second number : ";
    cin >> b;
    while (a > 0 && b > 0)
    {
        if (a > b)
        {
            a %= b;
        }
        else
        {
            b %= a;
        }
        nsk = a + b;
    }
    cout << "Your least common multiple : " << nsk;
    return 0;
}
