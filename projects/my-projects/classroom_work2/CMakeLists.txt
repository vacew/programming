#Common dependencies
if (LINUX)
 list(APPEND COMMON_DEPS m)
endif()
list(APPEND COMMON_DEPS common_math)

#1
add_subdirectory(1)
#2
add_subdirectory(2)
#3
add_subdirectory(3)
#4
add_subdirectory(4)
#5
add_subdirectory(5)
#6
add_subdirectory(6)
#7
add_subdirectory(7)
#8
add_subdirectory(8)
#9
add_subdirectory(9)
#10
add_subdirectory(10)