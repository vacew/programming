
#ifndef VECTOR3D_H_INCLUDED
#define VECTOR3D_H_INCLUDED
#include <stdio.h>
#include <string>

namespace demo {
    namespace math {
        namespace vector3d {
            class Vector3D
            {
            public:
                Vector3D(const double data[3]);
                virtual ~Vector3D();

                double length() const;

                double angleX() const;
                double angleY() const;
                double angleZ() const;

                Vector3D sum(const Vector3D& v) const;
                Vector3D subs(const Vector3D& v) const;
                Vector3D mult(const double& a) const;

                double   scalarMult(const Vector3D& v) const;
                Vector3D vectorMult(const Vector3D& v) const;
                double   angle(const Vector3D& v) const;

                std::string to_string() const;

                friend std::ostream& operator << (std::ostream&, const Vector3D&);
                friend const Vector3D operator +(const Vector3D&, const Vector3D&);
                friend const Vector3D operator -(const Vector3D&, const Vector3D&);
                friend const Vector3D operator *(const Vector3D&, const Vector3D&);
                friend const Vector3D operator *(const Vector3D&, const double&);
                friend const Vector3D operator *(const double&, const Vector3D&);

            protected:

            private:
                double data[3];
            };
        }
    }
}

#endif // VECTOR3D_H_INCLUDED