#ifndef STUDENTBUILDER_H
#define STUDENTBUILDER_H

#include "Student.h"

#include <string>
#include <iostream>



class StudentBuilder
{
public:

    Student build();
    StudentBuilder();
    virtual ~StudentBuilder();

    StudentBuilder& id(const long& id);
    StudentBuilder& speciality(const std::string& speciality);
    StudentBuilder& group(const std::string& group);
    StudentBuilder& course(const int& course);
    StudentBuilder& enter_date(const std::string& enter_date);
    StudentBuilder& leave_date(const std::string& leave_date);
    StudentBuilder& personal_info(PersonInfo* personal_info);
    StudentBuilder& contact_info(ContactInfo* contact_info);

private:
    Student student;
public:
    friend class Student;
};

#endif // STUDENTBUILDER_H

