#ifndef PERSONINFO_H
#define PERSONINFO_H

#include <string>
#include <iostream>
#include <sstream>

class PersonInfo
{
public:
    PersonInfo();
    PersonInfo(const std::string& f_name, const std::string& l_name, const std::string& b_date)
        : first_name(f_name)
        , last_name(l_name)
        , birth_date(b_date)
    {}
    virtual ~PersonInfo();

    std::string get_first_name()const { return this->first_name; }
    void set_first_name(const std::string&);

    std::string get_last_name()const { return this->last_name; }
    void set_last_name(const std::string&);

    std::string get_birth_date()const { return this->birth_date; }
    void set_birth_date(const std::string&);

    std::string to_string()const;
    friend std::ostream& operator <<(std::ostream& os, const PersonInfo& p);

private:
    std::string first_name;
    std::string last_name;
    std::string birth_date;
};

#endif // PERSONINFO_H