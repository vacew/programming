#ifndef STUDENT_H
#define STUDENT_H

#include <string>
#include <sstream>
#include <iostream>

#include "PersonInfo.h"
#include "ContactInfo.h"

class StudentBuilder;

class Student
{
public:

    virtual ~Student();

    long get_id()const { return this->id; }
    void set_id(const long&);

    std::string get_speciality()const { return this->speciality; }
    void set_speciality(const std::string&);

    std::string get_group() const { return this->group; }
    void set_group(const std::string&);

    int get_course()const { return this->course; }
    void set_course(const int&);

    std::string get_enter_date()const { return this->enter_date; }
    void set_enter_date(const std::string&);

    std::string get_leave_date()const { return this->leave_date; }
    void set_leave_date(const std::string&);


    const PersonInfo* get_personal_info()const;
    void set_personal_info(PersonInfo* p);

    const ContactInfo* get_contact_info()const;
    void set_contact_info(ContactInfo* c);

    std::string to_string()const;

private:
    long id;
    std::string speciality;
    std::string group;
    int course;
    std::string enter_date;
    std::string leave_date;
    PersonInfo* personal_info;
    ContactInfo* contact_info;
    Student() {}
public:
    friend class StudentBuilder;
    friend std::ostream& operator <<(std::ostream& os, const Student& s);

};


#endif // STUDENT_H