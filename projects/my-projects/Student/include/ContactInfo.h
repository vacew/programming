#ifndef CONTACTINFO_H
#define CONTACTINFO_H

#include <string>
#include <iostream>
#include <sstream>

class ContactInfo
{
public:
    ContactInfo();
    ContactInfo(const std::string& _address, const std::string& _t_number, const std::string& _e_mail)
        :address(_address)
        , t_number(_t_number)
        , e_mail(_e_mail)
    {}
    virtual ~ContactInfo();

    std::string get_address()const { return this->address; }
    void set_address(const std::string&);

    std::string get_t_number()const { return this->t_number; }
    void set_t_number(const std::string&);

    std::string get_e_mail()const { return this->e_mail; }
    void set_e_mail(const std::string&);

    std::string to_string()const;
    friend std::ostream& operator <<(std::ostream& os, const ContactInfo& p);
private:
    std::string address;
    std::string t_number;
    std::string e_mail;
};

#endif // CONTACTINFO_H
