#include "StudentBuilder.h"

StudentBuilder::StudentBuilder()
{
    //ctor
}

StudentBuilder::~StudentBuilder()
{
    //dtor
}
Student StudentBuilder::build()
{
    return student;
}
StudentBuilder& StudentBuilder::id(const long& id)
{
    student.set_id(id);
    return *this;
}
StudentBuilder& StudentBuilder::speciality(const std::string& speciality)
{
    student.set_speciality(speciality);
    return *this;
}
StudentBuilder& StudentBuilder::group(const std::string& group)
{
    student.set_group(group);
    return *this;
}
StudentBuilder& StudentBuilder::course(const int& course)
{
    student.set_course(course);
    return *this;
}
StudentBuilder& StudentBuilder::enter_date(const std::string& enter_date)
{
    student.set_enter_date(enter_date);
    return *this;
}
StudentBuilder& StudentBuilder::leave_date(const std::string& leave_date)
{
    student.set_leave_date(leave_date);
    return *this;
}
StudentBuilder& StudentBuilder::personal_info(PersonInfo* personal_info)
{
    student.set_personal_info(personal_info);
    return *this;
}
StudentBuilder& StudentBuilder::contact_info(ContactInfo* contact_info)
{
    student.set_contact_info(contact_info);
    return *this;
}