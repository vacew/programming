#include "ContactInfo.h"

ContactInfo::ContactInfo()
{
    //ctor
}

ContactInfo::~ContactInfo()
{
    //dtor
}
void ContactInfo::set_address(const std::string& address)
{
    this->address = address;
}
void ContactInfo::set_t_number(const std::string& t_number)
{
    this->t_number = t_number;
}
void ContactInfo::set_e_mail(const std::string& e_mail)
{
    this->e_mail = e_mail;
}
std::string ContactInfo::to_string()const
{
    std::stringstream sin;
    sin << "Contact information {address : " << address << "; telephone number : " << t_number << "; e-mail : " << e_mail << "}";
    return sin.str();
}
std::ostream& operator <<(std::ostream& os, const ContactInfo& c)
{
    os << c.to_string();
    return os;
}
