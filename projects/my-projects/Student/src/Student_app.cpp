#include <iostream>
#include <stdio.h>

#include "PersonInfo.h"
#include "ContactInfo.h"
#include "StudentBuilder.h"
#include "Student.h"
#include "Decanat.h"

using namespace std;

int main()
{
    PersonInfo personal_info("John", "Smith", "20.05.1995");

    ContactInfo contact_info("New York", "12412515125", "js1995@gmail.com");

    Student s1 = StudentBuilder()
        .id(12)
        .speciality("computer engineering")
        .group("105")
        .course(1)
        .enter_date("01.09.2019")
        .leave_date("20.05.2026")
        .personal_info(&personal_info)
        .contact_info(&contact_info)
        .build();

    cout << s1 << endl;

    return 0;
}