#include "PersonInfo.h"

PersonInfo::PersonInfo()
{
    //ctor
}

PersonInfo::~PersonInfo()
{
    //dtor
}

void PersonInfo::set_first_name(const std::string& first_name)
{
    this->first_name = first_name;
}
void PersonInfo::set_last_name(const std::string& last_name)
{
    this->last_name = last_name;
}
void PersonInfo::set_birth_date(const std::string& birth_date)
{
    this->birth_date = birth_date;
}
std::string PersonInfo::to_string() const
{
    std::stringstream sin;
    sin << "Personal information {name : " << first_name << "; surname : " << last_name << "; birth date :" << birth_date << "}";
    return sin.str();
}

std::ostream& operator <<(std::ostream& os, const PersonInfo& p)
{
    os << p.to_string();
    return os;
}