#include "Student.h"


Student::~Student()
{
    //dtor
}

const PersonInfo* Student::get_personal_info()const
{
    return personal_info;
}
const ContactInfo* Student::get_contact_info()const
{
    return contact_info;
}
void Student::set_speciality(const std::string& speciality)
{
    this->speciality = speciality;
}
void Student::set_id(const long& id)
{
    this->id = id;
}
void Student::set_group(const std::string& group)
{
    this->group = group;
}
void Student::set_course(const int& course)
{
    this->course = course;
}
void Student::set_enter_date(const std::string& leave_date)
{
    this->enter_date = enter_date;
}
void Student::set_leave_date(const std::string& leave_date)
{
    this->leave_date = leave_date;
}
void Student::set_personal_info(PersonInfo* personal_info)
{
    this->personal_info = personal_info;
}
void Student::set_contact_info(ContactInfo* contact_info)
{
    this->contact_info = contact_info;
}
std::string Student::to_string() const
{
    std::stringstream sin;
    sin << "Student {id : " << id << "; speciality : " << speciality << "; group : " << group << "; course : " << course << "; enter date :" << enter_date << "; leave date : " << leave_date;
    sin << "; Personal info : " << personal_info->to_string() << "; Contact info : " << contact_info->to_string() << "}";
    return sin.str();
}

std::ostream& operator <<(std::ostream& os, const Student& s)
{
    os << s.to_string();
    return os;
}