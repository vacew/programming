#ifndef FIGURE_H
#define FIGURE_H

#include <string>
#include <iostream>

class Figure
{
public:
    Figure();
    virtual ~Figure();

    virtual double area() const = 0;
    virtual double perimeter() const = 0;

    virtual std::string to_string()const;
    friend std::ostream& operator << (std::ostream& os, const Figure& f);

    virtual void validate()const;

private:
};

#endif // FIGURE_H
