#ifndef ELLIPSE_H
#define ELLIPSE_H

#include <iostream>
#include <sstream>
#include <string>

#include "Figure.h"
#include "FigureException.h"

#define M_PI 3.14;



class Ellipse : public Figure
{
public:
    Ellipse();
    Ellipse(double maxr, double minr);
    virtual ~Ellipse();

    double area() const override;
    double perimeter() const override;

    std::string to_string()const override;
    void validate()const override;

private:
    double maxr;
    double minr;
};

#endif // ELLIPSE_H