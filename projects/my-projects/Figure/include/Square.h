#ifndef SQUARE_H
#define SQUARE_H

#include <sstream>
#include <string>
#include <iostream>

#include "Figure.h"
#include "FigureException.h"

class Square : public Figure
{
public:
    Square();
    Square(double a);
    virtual ~Square();

    double area() const override;
    double perimeter()const override;


    std::string to_string()const override;

    void validate()const override;


private:
    double a;
};

#endif // SQUARE_H
