#ifndef PARALELOGRAM_H
#define PARALELOGRAM_H

#include <iostream>
#include <sstream>
#include <string>

#include "Figure.h"
#include "FigureException.h"

class Paralelogram : public Figure
{
public:
    Paralelogram();
    Paralelogram(double a, double b, double h);
    virtual ~Paralelogram();

    double area()const override;
    double perimeter()const override;

    std::string to_string()const override;

    void validate()const override;

private:
    double a;
    double b;
    double h;
};

#endif // PARALELOGRAM_H