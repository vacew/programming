#ifndef RECTANGLE_H
#define RECTANGLE_H

#include <stdio.h>
#include <string>
#include <sstream>

#include "Figure.h"
#include "FigureException.h"

class Rectangle : public Figure
{
public:
    Rectangle();
    Rectangle(double height, double weight);
    virtual ~Rectangle();

    double area()const override;
    double perimeter()const override;

    std::string to_string()const override;

    void validate()const override;

private:
    double height;
    double weight;
};

#endif // RECTANGLE_H

