#ifndef TRAPEZ_H
#define TRAPEZ_H

#include <iostream>
#include <sstream>
#include <string>

#include "Figure.h"
#include "FigureException.h"

class Trapez : public Figure
{
public:
    Trapez();
    Trapez(double upper_side, double down_side, double height, double left_side, double right_side);
    virtual ~Trapez();

    double area()const override;
    double perimeter()const override;

    std::string to_string()const override;

    void validate()const override;


private:
    double upper_side;
    double down_side;
    double height;
    double left_side;
    double right_side;
};

#endif // TRAPEZ_H
