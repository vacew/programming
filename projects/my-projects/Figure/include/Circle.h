#ifndef CIRCLE_H
#define CIRCLE_H

#include <sstream>
#include <iostream>
#include <string>

#include "Figure.h"
#include "FigureException.h"

#define M_PI 3.14;

class Circle : public Figure
{
public:
    Circle();
    Circle(double radius);
    virtual ~Circle();

    double area()const override;
    double perimeter()const override;

    std::string to_string()const override;

    void validate()const override;

private:
    double radius;

};

#endif // CIRCLE_H