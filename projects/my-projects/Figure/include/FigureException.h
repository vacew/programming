#ifndef FIGUREEXCEPTION_H
#define FIGUREEXCEPTION_H

//#include <exception>
#include <string>

class FigureException /*public std::exception*/
{
public:
    FigureException(const std::string& reason = "") :reason_(reason) {}

    virtual ~FigureException();

    virtual const std::string& reason()const
    {
        return this->reason_;
    }
private:
    std::string reason_;
};

class xNegativeParametr : public FigureException
{
public:
    xNegativeParametr(const std::string& reason = "") :FigureException(reason) {}

    virtual ~xNegativeParametr() {
    }
};
#endif // FIGUREEXCEPTION_H
