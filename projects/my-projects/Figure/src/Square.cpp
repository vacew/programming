#include "Square.h"
#include <iostream>
Square::Square()
{
    //ctor
}

Square::Square(double a_)
    :a(a_)
{
    validate();
}

Square::~Square()
{
    //dtor
}

double Square::area()const
{
    return a * a;
}
double Square::perimeter()const
{
    return 4 * a;
}
std::string Square::to_string()const
{
    std::stringstream sin;
    sin << "Square {Size of side = " << a << "; Area = " << area() << "; Perimeter = " << perimeter() << "}";
    return sin.str();
}
void Square::validate()const {
    if (this->a <= 0)
    {
        throw xNegativeParametr("Parameters should be positive");
    }
}