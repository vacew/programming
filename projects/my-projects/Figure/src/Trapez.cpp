#include "Trapez.h"

Trapez::Trapez()
{
    //ctor
}
Trapez::Trapez(double upper_side_, double down_side_, double height_, double left_side_, double right_side_)
    :upper_side(upper_side_)
    , down_side(down_side_)
    , height(height_)
    , left_side(left_side_)
    , right_side(right_side_)
{
    validate();
}
Trapez::~Trapez()
{
    //dtor
}
double Trapez::area()const
{
    return 0.5 * (upper_side + down_side) * height;
}
double Trapez::perimeter()const
{
    return left_side + upper_side + right_side + down_side;
}
std::string Trapez::to_string()const
{
    std::stringstream sin;
    sin << "Trapez {Upper side  = " << upper_side << "; Down side = " << down_side << "; Left side = " << left_side << "; Right side = " << right_side << ";";
    sin << " Height = " << height << "; Area = " << area() << "; Perimeter = " << perimeter() << "}";
    return sin.str();
}
void Trapez::validate()const {
    if (this->upper_side <= 0 || this->down_side <= 0 || this->height <= 0 || this->left_side <= 0 || this->right_side <= 0)
    {
        throw xNegativeParametr("Parameters should be positive");
    }
}