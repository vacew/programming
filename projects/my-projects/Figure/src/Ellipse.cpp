#include "Ellipse.h"

Ellipse::Ellipse()
{
    //ctor
}
Ellipse::Ellipse(double maxr_, double minr_)
    :maxr(maxr_)
    , minr(minr_)
{
    validate();
}
Ellipse::~Ellipse()
{
    //dtor
}
double Ellipse::area()const
{
    return maxr * minr * M_PI;
}
double Ellipse::perimeter()const
{
    return (maxr + minr) * M_PI;
}
std::string Ellipse::to_string()const
{
    std::stringstream sin;
    sin << "Ellipse {First radius = " << maxr << "; Second radius = " << minr << "; Area = " << area() << "; Perimeter = " << perimeter() << "}";
    return sin.str();
}
void Ellipse::validate()const {
    if (this->maxr <= 0 || this->minr <= 0)
    {
        throw xNegativeParametr("Parameters should be positive");
    }
}