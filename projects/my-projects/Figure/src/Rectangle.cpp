#include "Rectangle.h"

Rectangle::Rectangle()
{
    //ctor
}
Rectangle::Rectangle(double height_, double weight_)
    :height(height_)
    , weight(weight_)
{
    validate();
}
Rectangle::~Rectangle()
{
    //dtor
}
double Rectangle::area()const
{
    return height * weight;
}
double Rectangle::perimeter()const
{
    return 2 * (height + weight);
}
std::string Rectangle::to_string()const
{
    std::stringstream sin;
    sin << "Rectangle {Height = " << height << "; Weight = " << weight << "; Area = " << area() << "; Perimeter = " << perimeter();
    return sin.str();
}
void Rectangle::validate()const {
    if (this->height <= 0 || this->weight <= 0)
    {
        throw xNegativeParametr("Parameters should be positive");
    }
}

