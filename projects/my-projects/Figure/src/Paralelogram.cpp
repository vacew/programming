#include "Paralelogram.h"

Paralelogram::Paralelogram()
{
    //ctor
}
Paralelogram::Paralelogram(double a_, double b_, double h_)
    :a(a_)
    , b(b_)
    , h(h_)
{
    validate();
}

Paralelogram::~Paralelogram()
{
    //dtor
}
double Paralelogram::area()const
{
    return a * h;
}
double Paralelogram::perimeter()const
{
    return 2 * (a + b);
}
std::string Paralelogram::to_string()const
{
    std::stringstream sin;
    sin << "Paralelogram {Size of the first side = " << a << "; Size of the second side = " << b;
    sin << "; Height = " << h << "; Area = " << area() << "; Perimeter = " << perimeter() << "}";
    return sin.str();
}
void Paralelogram::validate()const {
    if (this->a <= 0 || this->b <= 0 || this->h <= 0)
    {
        throw xNegativeParametr("Parameters should be positive");
    }
}
