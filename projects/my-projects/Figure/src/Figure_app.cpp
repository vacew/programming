#include <iostream>
#include <stdio.h>

#include "Square.h"
#include "Ellipse.h"
#include "Paralelogram.h"
#include "Rectangle.h"
#include "Trapez.h"
#include "Circle.h"
#include "FigureException.h"

using namespace std;

int main()
{
    const Figure& f1 = Square(11);
    cout << "First shape(link) :" << endl;
    cout << "Area = " << f1.area() << endl;
    cout << "Perimeter = " << f1.perimeter() << endl;

    cout << "========================" << endl;

    Figure* f2 = new Square(4);
    cout << "Second shape(pointer) :" << endl;
    cout << "Area = " << f2->area() << endl;
    cout << "Perimeter = " << f2->perimeter() << endl;

    cout << "========================" << endl;
    try
    {
        Square s(-5);
        cout << s << endl;
    }
    catch (xNegativeParametr & ex)
    {
        cout << ex.reason() << endl;
    }
    catch (...) {
        std::cout << "Error" << std::endl;
    }
    cout << "========================" << endl;

    try
    {
        Ellipse e(5, 3);
        cout << e << endl;
    }
    catch (xNegativeParametr & ex)
    {
        cout << ex.reason() << endl;
    }

    cout << "========================" << endl;

    try
    {
        Paralelogram p(5, 6, 3);
        cout << p << endl;
    }
    catch (xNegativeParametr & ex)
    {
        cout << ex.reason() << endl;
    }

    cout << "========================" << endl;

    try
    {
        Rectangle r(7, 5);
        cout << r << endl;
    }
    catch (xNegativeParametr & ex)
    {
        cout << ex.reason() << endl;
    }

    cout << "========================" << endl;

    try
    {
        Trapez t(5, 7, 3, 2, 5);
        cout << t << endl;
    }
    catch (xNegativeParametr & ex)
    {
        cout << ex.reason() << endl;
    }

    cout << "========================" << endl;

    try
    {
        Circle c(5);
        cout << c << endl;
    }
    catch (xNegativeParametr & ex)
    {
        cout << ex.reason() << endl;
    }

    getchar();
    getchar();
    return 0;
}
