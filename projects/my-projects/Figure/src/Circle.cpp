#include "Circle.h"

Circle::Circle()
{
    //ctor
}
Circle::Circle(double radius_)
    :radius(radius_)
{
    validate();
}
Circle::~Circle()
{
    //dtor
}
double Circle::area()const
{
    return radius * radius * M_PI;
}
double Circle::perimeter()const
{
    return radius * 2 * M_PI;
}
std::string Circle::to_string()const
{
    std::stringstream sin;
    sin << "Circle { Radius = " << radius << "; Area = " << area() << "; Perimeter = " << perimeter() << "}";
    return sin.str();
}
void Circle::validate()const {
    if (this->radius <= 0)
    {
        throw xNegativeParametr("Parameters should be positive");
    }
}