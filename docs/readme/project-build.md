### Build projects on Windows using MinGW

To build project create folder `build/mingw` and go to it.

Using `git-bash` it shows as following

 * `mkdir -p build/mingw`
 * `cd build/mingw`

Next generate `MinGW Makefiles` using `cmake`
 * `cmake -G "MinGW Makefiles ../.."`

*Note* If the first-time attempt is fail then it needs to repeat this command.

![CMake first-time fail](../../images/cmake-first-time-fail.png)

After this type `mingw32-make` and wait for finishing build.

![CMake first-time fail](../../images/mingw32-make-example-1.png)


### Build projects on Linux

To build project create folder `build` and go to it.

After that perform the next command
`cmake ..` and `make`.

If all success then you could see `dist` sub-folder in `build`.

Navigate to it and you can see `bin` folder - contain executables and `lib` folder with libraries.
