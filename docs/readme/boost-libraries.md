### Boost library set
 `Boost` library is very popular framework to make professional solutions
 based on C++.
 
 #### Install on Windows
 
 `Boost` can be download from the official [site](https://www.boost.org/users/download/).
 Select latest version for windows and download `7z` archive (or `zip`).
 
 Also download pre-built library for [Visual Studio](https://sourceforge.net/projects/boost/files/boost-binaries/).
 
 There was downloaded `Boost` version [`1.72.0`](https://dl.bintray.com/boostorg/release/1.72.0/source/boost_1_72_0.7z).
 And pre-build version for Visual Studio 2019 [(msvc-14)](https://sourceforge.net/projects/boost/files/boost-binaries/1.72.0/boost_1_72_0-msvc-14.0-64.exe/download).
 
 Pre-built version could be installed to `C:\Program Files\boost\1.72.0\msvc`.
 
 If you plan to use MinGW (TDM-GCC) create folder `C:\Program Files\boost_1.72.0\mingw`.
 
 To simplify build and use Boost-libraries create environment variables
 
 ![Boost env variables](../../images/boost-env-vars.png)
 
 Next step 
 
 After downloading Boost archive unpack it into any directory on your PC and
 open GNU-console (for ex. `git-bash`) in the boost source root directory.
 
 After this type in console the following
 
 `./bootstrap.sh gcc`
 
 ![Boost bootstrap](../../images/boost-bootstrap.png)
 
 Wait for about minute and type the following command
 
 `./b2 -j8 toolset=gcc cxxstd=14 --build-type=complete --prefix="$BOOST_HOME\mingw" --layout=tagged address-model=64 link=static threading=multi install`
 
 ![Boost build](../../images/boost-b2-build.png)
 
 This command start build `Boost` in separated 4 threads (-j4) and install results of build to `$BOOST_MINGW64`
 
 Build process could take ~10-40 minutes depend from your PC.
 
 To rebuild `Boost` with other parameters should execute the following command
 
 `./b2 --clean-all -n`

Under `cmd` console these commands are
* `b2.exe -j8 toolset=gcc cxxstd=14 --build-type=complete --prefix="%BOOST_HOME%\mingw" --layout=tagged address-model=64 link=static threading=multi install`
* `b2.exe --clean-all -n` 

**NOTE** It's important to equal c++ standart on `CMAKE` and `Boost`

Make attention to important part of root `CMakeLists.txt` file

```
if (WIN32)
 list(APPEND X64_PLATFORMS AMD64 x86_64)
 list(APPEND X86_PLATFORMS x86)

 list(FIND X64_PLATFORMS ${CMAKE_SYSTEM_PROCESSOR} X64Found)
 list(FIND X86_PLATFORMS ${CMAKE_SYSTEM_PROCESSOR} X86Found)

 if (${X64Found} GREATER -1)
  set(BUILD_ARCH "x64")
  set (Boost_ARCHITECTURE "-x64")
 elseif(${X86Found} GREATER -1)
  set(BUILD_ARCH "x86")
  set (Boost_ARCHITECTURE "-x32")
 else()
  message(FATAL_ERROR "Unsupported target platform")
 endif()

 if (MINGW)
  set(BOOST_BUILD_TYPE "mingw")
 elseif(MSVC)
  set(BOOST_BUILD_TYPE "msvc")
  add_definitions (/W4)
 else()
  message(FATAL_ERROR "Unsupported build type")
 endif()

 set(BOOST_ROOT "$ENV{BOOST_HOME}\\${BOOST_BUILD_TYPE}")
endif() 
``` 
Here is checking of system architecture and build tools.
If you change `msvc` or `mingw` root placement change this part of `CMakeLists.txt`

#### Install Boost on Linux (Ubuntu)

Try to execute command `sudo apt install libboost-all-dev`

That's all :)