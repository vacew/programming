### Project description

Project contains root `CMakeLists.txt` that points to `projects` and `libs`
folders.

`project` folder contains all executable applications

`libs` folder - static or dynamic libraries

General structure of project
```
cpp_basic
|--CMakeLists.txt
|--projects 
|----CMakeLists.txt
|----simple_calc
|------CMakeLists.txt
|------...
|----compute_sequences
|------CMakeLists.txt
|------...
|----<your_own_project>
|------CMakeLists.txt
|------include
|--------<class.h>
|------src
|--------<project.cpp>
|--------<class.cpp>
|--libs
|----CMakeLists.txt
|----common_math
|------CMakeLists.txt
|------...
|--images
|---... 
``` 
