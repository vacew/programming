### Prerequests on Windows
Download and install
 * [CMake](https://cmake.org/download/) latest release.
 * [Git Bash](https://git-scm.com/download/win) (x86 or x64)
 * [TDM MinGW x64](https://github.com/jmeubank/tdm-gcc/releases/download/v9.2.0-tdm64-1/tdm64-gcc-9.2.0.exe)<br>
   or
   [TDM MinGW x86](https://github.com/jmeubank/tdm-gcc/releases/download/v9.2.0-tdm-1/tdm-gcc-9.2.0.exe) 

Setup all of these tools or unarchive if you download them as `zip`-archives.
  
Go to `Control Panel > User Accounts`
  
![User accounts management](../../images/cp_user_accounts.png)
  
Then click to `User Accounts` and on the left-side panel click on `Change my environment variable`
  
![Environment variables](../../images/cp_user_accounts_det.png)
  
On the top side of appeared dialog click `New...` button and add `CMAKE_HOME` variable
that point to a root folder which contains `CMake` executables files.
  
![CMake env variable](../../images/path_cmake_var.png)
  
Repeat this step for `Git` - create `GITBASH_HOME` env variable that points to root Git folder<br>
and `MinGW` - create `MINGW_HOME` env variable that points to root `TDM-GCC-64` or `TDM-GCC` folder.
  
Example of setup variables was shown below

![Example of env vars](../../images/path_vars.png)

*Note* Don't forget to add `%MINGW_HOME%\bin;%GITBASH_HOME%;%CMAKE_HOME%\bin` at the end of `PATH` variable.  
  
If you have done everything correct that you can check availability of tools.

First of all open any folder in `explorer` and type `git-bash` in address line. You could see `git-bash` terminal.

In this console type the following commands
* `cmake -version`
* `mingw32-make -version`

You could see the following

![Checking availability of tools](../../images/tools-avail-check.png)

### Prerequests on Linux (Ubuntu, Kubuntu)
Execute the following command to update system `sudo apt-get -y update`<br>
Execute the following command `sudo apt-get -y install build-essential git cmake`

After installation check the CMake version `cmake -version`
   